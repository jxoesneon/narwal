# ![ ](https://i.ibb.co/3rb752n/logo32x32.png) Narwhal Run

## Proyect description

This is a simple Side scroller game written in Python and leveraging the Pygame module.

_The main objective of the game is to get the highest score possible by escaping the hungry orca
that will chase you relentlessly, all while trying to avoid the obstacles the world has thrown your way._

## Where to get

[Bitbucket](https://bitbucket.org/jxoesneon/narwal/downloads/)

## Requirements

* Operating System
    * Windows 7 (SP3) and later
    * OS X 10.9 and later
    * Ubuntu 18.0 and later

* Python >= 3.8
    * Pygame >= 2.0.0.dev6
    * Pillow >= 6.2.1

### Quick install

#### 1. Python

* For windows
    * [32-bits](https://www.python.org/ftp/python/3.8.0/python-3.8.0-webinstall.exe)
    * [64-bits](https://www.python.org/ftp/python/3.8.0/python-3.8.0-amd64-webinstall.exe)
* For MacOS
    * [64-bits](https://www.python.org/ftp/python/3.8.0/python-3.8.0-macosx10.9.pkg)
* For ubuntu/debian base systems
    * In a terminal:

```bash

sudo apt-get update
sudo apt-get upgrade
sudo wget https://www.python.org/ftp/python/3.8.0/Python-3.8.0.tgz
sudo apt-get install build-essential checkinstall
sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev
cd /opt
sudo tar xzf Python-3.8.0.tgz
cd Python-3.8.0
sudo ./configure --enable-optimizations
sudo make altinstall
cd /opt
sudo rm -f Python-3.8.0.tgz
sudo apt-get install python-pip
python3.8 -m pip install --upgrade pip
```

#### 2. Modules (platform independent)

```bash
python -m pip install pygame>=2.0.0.dev6 pillow>=6.2.1 requests
```

## Game installation

* Go to [Bitbucket](https://bitbucket.org/jxoesneon/narwal/downloads/)
* Click on _"Download Repository"_
* Extract the downloaded file to a directory of your liking
* Open the extracted folder
* Double click on the "Main.py"
* **Have Fun!**

## Changelog

### Narwal

|**Author**|**Commit**|**Message**|**Date**|
|:----------:|:----------:|:----------:|:----------:|
Eduardo Rojas|d398674|New material and performance optimization|2019-11-28|
|Eduardo Rojas|84cb22a|Updated cave model with textures and new design|2019-11-28|
|Eduardo Rojas|e3aab91|Now ignoring jpgs as they are not supported|2019-11-28|
|Eduardo Rojas|9941d23|Added game music|2019-11-27|
|Eduardo Rojas|4da61ed|Added main app's default font|2019-11-27|
|Eduardo Rojas|3db2d7a|Created res/swimming animations|2019-11-27|
|Eduardo Rojas|621a4b2|Created res/surfacing animations|2019-11-27|
|Eduardo Rojas|06ff7d2|Created res/idle animations|2019-11-27|
|Eduardo Rojas|ee3fcb2|alternate bg for sandbox|2019-11-27|
|Eduardo Rojas|48e2448|defauld sprite for main character|2019-11-27|
|Eduardo Rojas|1db662e|3d model for the main character|2019-11-27|
|Eduardo Rojas|fc0be30|img for locked levels in level selection screen|2019-11-27|
|Eduardo Rojas|80ea6ba|sanbox bg|2019-11-27|
|Eduardo Rojas|c5b55ab|Main app's icon|2019-11-27|
|Eduardo Rojas|69dc7ca|3d model of background for level 1|2019-11-27|
|Eduardo Rojas|bd1b7c4|3d model for the sanbox background|2019-11-27|
|Eduardo Rojas|6ca52cf|Created main app background|2019-11-27|
|Eduardo Rojas|1bc4534|no longer tracking blender backups|2019-11-27|
|Eduardo Rojas|ee66a8a|Created Sandbox Level|2019-11-27|
|Eduardo Rojas|0668d54|Created main application|2019-11-27|
|Eduardo Rojas|1a0aad8|Created gitignore|2019-11-27|
|Eduardo Rojas|e036698|Initial commit|2019-11-27|

### Narwal Run

|**Author**|**Commit**|**Message**|**Date**|
|:----------:|:----------:|:----------:|:----------:|
|Eduardo Rojas|fb4f64d|Updated res/swimming animations Former-commit-id: 609c431f3c8f2615d708db8b0bfe535d515834a6|2019-11-27|
|Eduardo Rojas|7334eef|Updated res/surfacing animation Former-commit-id: 34a8a61a4c57c66bde47a271521569ef9532cea7|2019-11-27|
|Eduardo Rojas|a428fca|created res/idle animations Former-commit-id: 9d179f00d4f31fdc1e4187757e8e50bc1c8a2244|2019-11-27|
|Eduardo Rojas|a0f5955|created cave for sandbox Former-commit-id: c9837c2cf825256491c27e0c9e725282dfed5592|2019-11-27|
|Eduardo Rojas|d3e819e|made resizing work Former-commit-id: 5304d45ef2cb10cfd60171e90433b15b2c8df79f|2019-11-27|
|Eduardo Rojas|e289ddc|now ignoring default res/norman.png Former-commit-id: 319a6d160e0bc1d09d9e407c2fb1ebb48800cd47|2019-11-27|
|Eduardo Rojas|22842e0|Updated gitignore to manage large files in repo Former-commit-id: 0ec4be0fe1603437b5b1499effea2ef5ca890e75|2019-11-27|
|Eduardo Rojas|9d4ceee|WIP: resizeable screen Former-commit-id: f6f5af38170e5cf2b90654d1dbb6956c3d7587ff|2019-11-25|
|Eduardo Rojas|f4619f7|Handled animation states, added speed boost Former-commit-id: 6b856bb58f0d9df558d88998af3e1b05391fa705|2019-11-25|
|Eduardo Rojas|2602b92|Added the surfacing animation Former-commit-id: b2f1a14b80b454b128bd5c065e4a074ffc753593|2019-11-25|
|Eduardo Rojas|05859b6|removed unnecessary call and whitespace Former-commit-id: 588903590504192b7b1251d62c5d3a60a6c6a975|2019-11-24|
|Eduardo Rojas|1cf8b1e|added support for animations, they are pulled auto Former-commit-id: 780801e4a77149b1a2b6791c4d9badf703ffdb23|2019-11-24|
|Eduardo Rojas|a0aac63|Added idle animation Former-commit-id: de5b576262cf030598b1c67924c039d4cd697f6a|2019-11-24|
|Eduardo Rojas|eddefef|Created standalone sandbox demo Former-commit-id: faea6903265448ccd0cddde75da3b4cf41927726|2019-11-24|
|Eduardo Rojas|d62beb1|Created animations Former-commit-id: cfb89dfcb36e4b719e34955e6b1a6331b347091c|2019-11-24|
|Eduardo Rojas|27babae|deleted backup since source control takes care Former-commit-id: 66446f9fa4638d0d285bee8568a0ebcc2c2159a0|2019‑11‑21|
|Eduardo Rojas|baf31c6|rexported Former-commit-id: 9dca90f8355dd3b110af35a74b3a57b523b3f34a|2019‑11‑21|
|Eduardo Rojas|b707b6e|removed gloval variables in favor of instance Former-commit-id: 851261c8d3f3a4389feb2aa5fc5895cf66ce7167|2019‑11‑21|
|Eduardo Rojas|1e639e8|croped norman to only take as much space as needed Former-commit-id: f27ae91ae2f8f4871a062016a9e539258fa0f54a|2019‑11‑21|
|Eduardo Rojas|e0849b3|Created Game clase and work on updatind Norman Former-commit-id: 3bc1431516efd5925fd02d42ebf981bdef9a2926|2019‑11‑20|
|Eduardo Rojas|fe3bca4|PEP8 compliance Former-commit-id: 1d2f4b1e6724a6985754dfd1e0c86658dac4f568|2019‑11‑20|
|Eduardo Rojas|fca653f|Code restructure Former-commit-id: 62d55001d4d20d2742f2f3665c48d41679ed39cb|2019‑11‑20|
|Eduardo Rojas|b4a1319|WIP sandbox level start Former-commit-id: 6f050b655269c942fb0cb84c5bc6a19862c4493e|2019‑11‑19|
|Eduardo Rojas|0241bd7|WIP sandbox level select Former-commit-id: 326693bbd4af6fadba926a895d37f301dfac52d6|2019‑11‑18|
|Eduardo Rojas|3c6ecd4|Created update function to draw norman on screen Former-commit-id: 4a2192e1c2db1a3cc8d8bb18bdfa2ccd9c1176c2|2019‑11‑18|
|Eduardo Rojas|4bec5cf|Created main character sprite Former-commit-id: 6453084a1b54d706b7b9e198cd352f4b9fb23dbc|2019‑11‑18|
|Eduardo Rojas|0468792|Converted audio files to ogg for easy manipulation Former-commit-id: b7fc77a3bb97a4b5e2c9169577399d51f7352654|2019‑11‑18|
|Eduardo Rojas|3f334ab|WIP func for the main character Former-commit-id: d6c92ed91641c0c34f6ef85150dfd384c184ae9a|2019‑11‑18|
|Eduardo Rojas|6387e60|WIP modified landscape material Former-commit-id: d6628ff7c01930041e02e754936f8fb71c2e951f|2019‑11‑18|
|Eduardo Rojas|f85dcee|file for blender 2.79 Former-commit-id: 57ce8eda6809c0294d3855acb044743db7ebd008|2019‑11‑18|
|Eduardo Rojas|6f6d249|Updated material for main char and WIP animations Former-commit-id: 355e3ab4e5a183e2bec514860c78bd8c4fa284f0|2019‑11‑18|
|Eduardo Rojas|4f3dd38|added background img for sandbox level Former-commit-id: dc393df6ab9997a1282c410e9ee07bcb4043a0fa|2019‑11‑18|
|Eduardo Rojas|9c416ca|ignored vscode settings Former-commit-id: a445be95323a8498f299a65fec3c38b121ef43dc|2019‑11‑16|
|Eduardo Rojas|f24a62b|Updated lanscape material Former-commit-id: 3af62dc3e0497d13c5feb23be8ec7152299fea86|2019‑11‑16|
|Eduardo Rojas|b7b9451|Corrected paths to work in diferent OS Former-commit-id: 3fe76058f7f456b916c9146006cab7ede25dcbec|2019‑11‑16|
|Eduardo Rojas|9ea65d9|Created 3d lanscape Former-commit-id: 319429f8aff961df7e418d103b10a85efb3ec61e|2019‑11‑14|
|Eduardo Rojas|eb25bc4|added option to level_select and created colission|2019‑11‑13|
|Eduardo Rojas|1eb9406|added locked level img|2019‑11‑13|
|Eduardo Rojas|1e744e2|moved editable assets|2019‑11‑13|
|Eduardo Rojas|475eda2|Created level selection screen and added music|2019‑11‑13|
|Eduardo Rojas|638c2e3|Added sound for start screen and first level|2019‑11‑13|
|Eduardo Rojas|505c878|Functions to support displaying messages on screen|2019‑11‑12|
|Eduardo Rojas|dcc2c58|Added main font for start screen|2019‑11‑12|
|Eduardo Rojas|d738383|Created blender asset for main character|2019‑11‑12|
|Eduardo Rojas|3522cdf|Created Variables for the screen size and gm clk|2019‑11‑12|
|Eduardo Rojas|bfb655a|Added files to be ignored|2019‑11‑10|
|Eduardo Rojas|56e7499|Initial Code to launch app|2019‑11‑10|
|Eduardo Rojas|9bf17db|SVG of main Character|2019‑11‑10|
|Eduardo Rojas|0b29484|Start Screen Background|2019‑11‑10|
|Eduardo Rojas|cac83ea|Application Logo|2019‑11‑10|
|Eduardo Rojas|9def1c3|Initial commit|2019‑11‑10|
