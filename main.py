import pygame
from os.path import join as xpath
from PIL import Image

# Global Color variables
black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)

# load and set the logo
logo_path = xpath("assets", "logo32x32.png")
logo = pygame.image.load(logo_path)
pygame.display.set_icon(logo)
pygame.display.set_caption("Narwhal Run")

# set up game window variables
window_height = 707
window_width = 1366
window_size = (window_width, window_height)
window_center_x = window_width/2
window_center_y = window_height/2
window_center = (window_center_x, window_center_y)
screen = pygame.display.set_mode(window_size)

# set the game clock
clock = pygame.time.Clock()
clock.tick(60)


# sound variables
volume = 50
mute = True


class Game:
    # define a variable to control the main loop
    current_level = None
    resolution = 512
    running = True

    def screen_fade(self, speed=5, step=1):
        fade = pygame.Surface(window_size)
        fade.fill((0, 0, 0))
        for alpha in range(0, 300, step):
            fade.set_alpha(alpha)
            pygame.display.update()
            screen.blit(fade, (0, 0))
            pygame.display.update()
            pygame.time.delay(speed)

    def add_image(self, path, loc=(0, 0), scale=(0, 0)):
        img_path = f"{path}"
        img = pygame.image.load(img_path)
        meta = Image.open(img_path)
        dimensions = meta.size
        ini_x, ini_y = loc
        if scale != (0, 0):
            fin_x, fin_y = scale
            img = pygame.transform.scale(img, (fin_x, fin_y))
        else:
            fin_x, fin_y = dimensions
        end_x = ini_x + fin_x
        end_y = ini_y + fin_y
        bounds = (ini_x, ini_y, end_x, end_y)
        screen.blit(img, loc)
        return(bounds)

    def add_background(self):
        Narwhal.add_image(xpath("assets", "background.png"))
        message_display("Narwhal Run", 120, white, from_center(0, -30), True)
        message_display(
                        "Press Enter to Start",
                        40,
                        white,
                        from_center(0, 60),
                        True
                        )
        pygame.display.flip()


Narwhal = Game()


def start_screen():
    pygame.mixer.music.load(xpath("assets", "music", "start_screen.ogg"))
    if not mute:
        pygame.mixer.music.set_volume(volume)
    else:
        pygame.mixer.music.set_volume(0)
    pygame.mixer.music.play(-1)
    Narwhal.add_background()
    while Narwhal.running:
        for event in pygame.event.get():
            if event.type == pygame.KEYUP:
                press_enter = event.key == pygame.K_KP_ENTER
                press_return = event.key == pygame.K_RETURN
                if press_enter or press_return:
                    pygame.mixer.music.pause()
                    pygame.mixer.Sound(
                                        xpath(
                                                "assets",
                                                "music",
                                                "press_enter.ogg"
                                                )
                                        ).play()
                    pygame.mixer.music.unpause()
                    Narwhal.screen_fade(speed=1, step=2)
                    Narwhal.current_level = level_selection()


def game_loop():
    # main loop
    while Narwhal.running:
        # event handling, gets all event from the event queue
        for event in pygame.event.get():
            # only do something if the event is of type QUIT
            if event.type == pygame.QUIT:
                # change the value to False, to exit the main loop
                Narwhal.running = False
        Narwhal.current_level = start_screen
        Narwhal.current_level()


def level_selection():
    Narwhal.add_image(xpath("assets", "background.png"))
    level_1 = Narwhal.add_image(
                        xpath("assets", "locked_level.png"),
                        loc=from_center(-225, -250)
                        )
    level_2 = Narwhal.add_image(
                        xpath("assets", "locked_level.png"),
                        loc=from_center(25, -250)
                        )
    level_sandbox = Narwhal.add_image(
                              xpath("assets", "sandbox.png"),
                              loc=(0, (window_height-75)),
                              scale=(100, 75)
                              )
    message_display("Select a level", 40, white, (window_center_x, 45), True)
    while Narwhal.running:
        # event handling, gets all event from the event queue
        for event in pygame.event.get():
            mouse_click = event.type == pygame.MOUSEBUTTONDOWN
            # only do something if the event is of type QUIT
            if event.type == pygame.QUIT:
                # change the value to False, to exit the main loop
                Narwhal.running = False
            if mouse_click and pygame.mouse.get_focused():
                if colission(event.pos, level_1):
                    message_display("selected level 1",
                                    40,
                                    loc=(window_center_x, 45)
                                    )
                if colission(event.pos, level_2):
                    message_display("This level has not been unlocked yet.",
                                    40,
                                    loc=(window_center_x, 45)
                                    )
                if colission(event.pos, level_sandbox):
                    Narwhal.current_level = sandbox_level()
        pygame.display.update()


class Player():

    def __init__(self):
        self.x1 = 0
        self.y1 = 0
        self.pos = (self.x1, self.y1)
        self.source = xpath("assets", str(Narwhal.resolution), "norman.png")
        self.x2, self.y2 = (Image.open(self.source)).size
        self.center = ((self.x2 / 2), (self.y2 / 2))
        self.sprite = pygame.image.load(self.source)
        self.bounds = (self.x1, self.y1, self.x2, self.y2)
        super().__init__()

    def update(self, pos):
        self.x1, self.y1 = pos
        self.pos = pos
        Narwhal.add_image(self.source, loc=self.pos)


def sandbox_level():
    exit_sandbox = False
    Narwhal.screen_fade(1, 2)
    move_amnt = 3
    Narwhal.add_image(xpath("assets", "sandbox.png"))
    while Narwhal.running and not exit_sandbox:
        Norman = Player()
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP or pygame.K_w:
                    Norman.x1 += move_amnt
                if event.key == pygame.K_DOWN or pygame.K_s:
                    Norman.x1 -= move_amnt
                if event.key == pygame.K_LEFT or pygame.K_a:
                    Norman.y1 += move_amnt
                if event.key == pygame.K_RIGHT or pygame.K_d:
                    Norman.y1 -= move_amnt
                Norman.update((Norman.x1, Norman.y1))
        pygame.display.update()


def text_objects(text, font, color):
    TextSurface = font.render(text, True, color)
    return TextSurface, TextSurface.get_rect()


def message_display(message, size, color=white, loc=(0, 0), outline=False):
    if outline:
        TextFont = pygame.font.Font(
                                    xpath(
                                          "assets",
                                          "fonts",
                                          "SEA_GARDENS.ttf"
                                          ),
                                    (size + 1)
                                    )
        TextSurf, TextRect = text_objects(message, TextFont, black)
        TextRect.center = loc
        screen.blit(TextSurf, TextRect)
        TextFont = pygame.font.Font(
                                    xpath(
                                          "assets",
                                          "fonts",
                                          "SEA_GARDENS.ttf"
                                          ),
                                    size
                                    )
        TextSurf, TextRect = text_objects(
                                          message,
                                          TextFont,
                                          color
                                          )
        TextRect.center = loc
        screen.blit(TextSurf, TextRect)
    else:
        TextFont = pygame.font.Font(
                                    xpath(
                                          "assets",
                                          "fonts",
                                          "SEA_GARDENS.ttf"
                                          ),
                                    size
                                    )
        TextSurf, TextRect = text_objects(message, TextFont, color)
        TextRect.center = loc
        screen.blit(TextSurf, TextRect)


def from_center(delta_1=0, delta_2=0):
    pos_x, pos_y = window_center
    pos_new = (pos_x + delta_1, pos_y + delta_2)
    return pos_new


def colission(pos, bounds):
    pos_x, pos_y = pos
    bound_x1, bound_y1, bound_x2, bound_y2 = bounds
    x_overlap = False
    y_overlap = False
    inside = False
    if pos_x >= bound_x1 and pos_x <= bound_x2:
        x_overlap = True
    if pos_y >= bound_y1 and pos_y <= bound_y2:
        y_overlap = True
    if x_overlap and y_overlap:
        inside = True
    return inside


def screen_pos(img_path, *args):
    meta = Image.open(img_path)
    end_x, end_y = meta.size
    top_left = (0, 0)
    top_center = ((window_center_x - (end_x/2)), 0)
    top_right = ((window_width - end_x), 0)
    center_left = (0, (window_center_y - (end_y/2)))
    center_center = (
                     (window_center_x - (end_x/2)),
                     (window_center_y - (end_y/2))
                     )
    center_right = ((window_width - end_x), (window_center_y - (end_y/2)))
    bottom_left = (0, (window_height - end_y))
    bottom_center = ((window_center_x - (end_x/2)), (window_height - end_y))
    bottom_right = ((window_width - end_x), (window_height - end_y))


if __name__ == "__main__":
    # Start the game window
    pygame.init()
    # start the music service
    pygame.mixer.init()
    # start the game loop
    game_loop()
