from os import walk as xwalk, environ
environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (0,30)
import pygame
from os.path import join as xpath
# from os.path import splitext
from PIL import Image
# from detect_collission import detect_collission

running = True
clock = pygame.time.Clock()


class Screen():
    def __init__(self):
        self.displaying = False
        self.debuggin = False
        self.resolution = self.get_res()
        self.center = self.get_center()
        self.adj_res = self.adjust_res(self.resolution)
        self.screen = pygame.display.set_mode(
                                              self.resolution,
                                              pygame.RESIZABLE
                                              )
        self.bg_img_start_pos = (0, 0)
        self.get_border()
        self.bg_img = pygame.image.load(xpath("assets", "sandbox.png"))
        super().__init__()

    def get_center(self):
        center_x = int(self.resolution[0]/2)
        center_y = int(self.resolution[1]/2)
        return (center_x, center_y)

    def get_border(self):
        self.border_percentage = .10
        self.border_pos_x1 = int((self.resolution[0] * self.border_percentage)/4)
        self.border_pos_y1 = int((self.resolution[1] * self.border_percentage)/2)
        self.border_pos_x2 = self.resolution[0] - (self.border_pos_x1*16)
        self.border_pos_y2 = self.resolution[1] - (self.border_pos_y1*8)
        self.border_bounds = (
                              self.border_pos_x1,
                              self.border_pos_y1,
                              self.border_pos_x2,
                              self.border_pos_y2
                              )
        return self.border_bounds

    def get_res(self):
        try:
            res = self.screen.get_surface().get_size()
            self.resolution = res
        except:
            print("Unable to get window Resolution, setting defaults...")
            res = (1366,707)
            self.resolution = res
        return res

    def adjust_res(self, res):
        y = res[1]
        if 0 <= y <= 768:
            return 256
        elif 768 <= y <= 1024:
            return 512
        elif 1024 <= y <= 1440:
            return 768
        elif 1440 <= y <= 2160:
            return 1024
        elif y == 2160:
            return 1440
        elif y <= 2160:
            return 2160
        else:
            raise AssertionError("Could not detect the screen resolution")

    def update(self):
        self.displaying = True
        image = Norman.get_anim(Norman.state, Norman.frame)
        if self.debuggin:
            pygame.draw.rect(self.screen, (0,255,0),
                            (self.get_border())
                            )
            pygame.draw.rect(self.screen, (255,0,0),
                            Norman.get_bounds()
                            )
            pygame.draw.rect(self.screen, (255,0,255),(0,0,10,10))
            pygame.draw.rect(self.screen, (255,0,255),
                            (self.border_pos_x2,
                            self.border_pos_y2,
                            self.resolution[0],
                            self.resolution[1]
                            )
                            )
        if image is not None:
            surface = pygame.image.load(image)
        else:
            path = xpath("assets", self.adj_res, "idle", "0000.png")
            surface = pygame.image.load(path)
        regular_anim = ["swimming_r", "idle", "surfacing"]
        flipped_anim_x = ["swimming_l"]
        flipped_anim_y = [""]
        flipped_anim_x_y = ["diving"]

        if Norman.state in regular_anim:
            self.screen.blit(surface, (Norman.x1, Norman.y1))

        if Norman.state in flipped_anim_x:
            flipped = pygame.transform.flip(surface, True, False)
            self.screen.blit(flipped, (Norman.x1, Norman.y1))

        if Norman.state in flipped_anim_y:
            flipped = pygame.transform.flip(surface, False, True)
            self.screen.blit(flipped, (Norman.x1, Norman.y1))

        if Norman.state in flipped_anim_x_y:
            flipped = pygame.transform.flip(surface, True, True)
            self.screen.blit(flipped, (Norman.x1, Norman.y1))

        pygame.display.update()
        clock.tick(60)


Main_Window = Screen()


def animation():
    path = xpath("assets", str(Main_Window.adj_res))
    animations = {}
    for root, dirs, files in xwalk(path):
        for anim in dirs:
            new_path = xpath(root, anim)
            for n_root, n_dirs, n_files in xwalk(new_path):
                path_list = []
                for n_file in n_files:
                    final_path = xpath(n_root, n_file)
                    path_list.append(final_path)
                animations[anim] = path_list
    return animations


class Player():

    def __init__(self):
        self.acceleration = 5
        self.state = "idle"
        self.warping = False
        self.warp_count = 0
        self.frame = 0
        self.idle_count = 0
        self.img_path = xpath(
                              "assets",
                              str(Main_Window.adj_res),
                              "idle",
                              "0000.png"
                              )
        self.img_info = Image.open(self.img_path)
        self.size = self.img_info.size
        self.x1 = int(((Main_Window.resolution[0] * 0.1) / 4) + 1)
        self.y1 = int(((Main_Window.resolution[1] * 0.1) / 2) + 1)
        self.x2 = self.x1 + self.size[0]
        self.y2 = self.y1 + self.size[1]
        self.bounds = self.get_bounds()
        self.img = pygame.image.load(self.img_path)
        self.last_movement = ""
        # super().__init__()

    def get_bounds(self):
        return (self.x1,self.y1,self.x2,self.y2)

    def get_anim(self, action, frame_number):
        ani_set = animation()
        if action == "swimming_r" or action == "swimming_l":
            action = "swimming"
        if action == "surfacing" or action == "diving":
            action = "surfacing"
        if action in ani_set:
            frames = ani_set[action]
            frames.sort()
            if frames:
                img_frame = frames[frame_number]
                return img_frame
            else:
                return frames[0]


def detect_collission(obj, bounds, inverted=False):
    obj_x1, obj_y1, obj_x2, obj_y2 = obj
    bound_x1, bound_y1, bound_x2, bound_y2 = bounds
    x1_overlap = bool
    y1_overlap = bool
    x2_overlap = bool
    y2_overlap = bool
    collision = bool
    if inverted:
        if bound_x2 <= obj_x1 or obj_x1 <= bound_x1 or bound_x1 >= obj_x2 or obj_x2 >= bound_x2:
            collision = True
        elif bound_y2 <= obj_y1 or obj_y1 <= bound_y1 or bound_y1 >= obj_y2 or obj_y2 >= bound_y2:
            collision = True
        else:
            collision = False
    else:
        if obj_x1 >= bound_x1 and obj_x1 <= bound_x2:
            x1_overlap = True
        if obj_y1 >= bound_y1 and obj_y1 <= bound_y2:
            y1_overlap = True

        if obj_x2 >= bound_x1 and obj_x2 <= bound_x2:
            x2_overlap = True
        if obj_y2 >= bound_y1 and obj_y2 <= bound_y2:
            y2_overlap = True


        if (x1_overlap and y1_overlap) or (x2_overlap and y2_overlap):
            collision = True
        else:
            collision = False
    return collision


Norman = Player()

def scren_scroll(player_bounds):
    player_collided = detect_collission(player_bounds,
                                        (
                                         Main_Window.get_border()
                                         ),
                                        inverted=True
                                        )

    if player_collided:
        Norman.warp_count += 1
        if Norman.warp_count>1:
            x, y  = Main_Window.get_center()
            xdif = int( x - Norman.x1)
            ydif = int( y - Norman.y1)

            if Norman.x1 < x:
                Norman.x1 += int((xdif)*.01)
            else:
                Norman.x1 -= int((xdif)*.01)

            if Norman.y1 > y:
                Norman.y1 += int((ydif)*.05)
            else:
                Norman.y1 -= int((ydif)*.05)

            Norman.warp_count -= 2 if Norman.warp_count>0 else 0
        return True
    else:
        Norman.warp_count -= 1 if Norman.warp_count>0 else 0
        return False

def game_loop():
    pygame.init()
    global running
    scroll_ammount = 5
    while running:
        scrolling = scren_scroll(Norman.get_bounds())
        if scrolling:
            if Norman.last_movement == "left":
                Norman.x1 += Norman.acceleration
            if Norman.last_movement == "right":
                Norman.x1 -= Norman.acceleration
            if Norman.last_movement == "up":
                Norman.y1 += Norman.acceleration
            if Norman.last_movement == "down":
                Norman.y1 -= Norman.acceleration
        else:
            Norman.acceleration = Norman.acceleration
            Main_Window.screen.blit(
                                    Main_Window.bg_img,
                                    Main_Window.bg_img_start_pos
                                    )

        keys = pygame.key.get_pressed()
        Norman.state = "idle" if Norman.idle_count > 90 else Norman.state


        if keys[pygame.K_LSHIFT] or keys[pygame.K_RSHIFT]:
            Norman.idle_count = 0
            Norman.acceleration = Player().acceleration*3
        else:
            Norman.acceleration = Player().acceleration


        if keys[pygame.K_UP] or keys[pygame.K_w]:
            Norman.idle_count = 0
            Norman.state = "surfacing"
            Norman.y1 -= Norman.acceleration if not scrolling else 0
            Norman.last_movement = "up"

        if keys[pygame.K_DOWN] or keys[pygame.K_s]:
            Norman.idle_count = 0
            Norman.state = "diving"
            Norman.y1 += Norman.acceleration if not scrolling else 0
            Norman.last_movement = "down"

        if keys[pygame.K_LEFT] or keys[pygame.K_a]:
            Norman.idle_count = 0
            Norman.state = "swimming_l"
            Norman.x1 -= Norman.acceleration if not scrolling else 0
            Norman.last_movement = "left"

        if keys[pygame.K_RIGHT] or keys[pygame.K_d]:
            Norman.idle_count = 0
            Norman.state = "swimming_r"
            Norman.x1 += Norman.acceleration if not scrolling else 0
            Norman.last_movement = "right"


        if Norman.frame < 60:
            Norman.frame += 1
        else:
            Norman.frame = 0
        Norman.idle_count += 1

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.VIDEORESIZE:
                Main_Window.resolution = (event.w, event.h)
                Main_Window.screen = pygame.display.set_mode(
                                                             Main_Window.resolution,
                                                             pygame.RESIZABLE
                                                             )
        Main_Window.update()


if __name__ == "__main__":
    game_loop()
